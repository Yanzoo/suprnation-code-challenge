# SuprNation Code Challenge

##Prerequisites
Make sure to have the following tools installed on your machine : 
- node >= 10.16.2
- npm >= 6.9.0


##Installing and Running
- clone repo on local machine: git clone https://Yanzoo@bitbucket.org/Yanzoo/suprnation-code-challenge.git
- From your terminal/ cmd, navigate to the path where the repo has been cloned and then run the following commands
  cd suprnation-code-challenge
  npm install && npm start
 
The project should automatically load on your browser. In case it does not, navigate to http://localhost:3000


##Folder Structure
**public** - public files including our page (index.html)

**src/assets** - Currently emply but may include an image sub folder holding images such as the logo

**src/components** - Currently holds Keypad element only. This folder may hold UI components, Navigation related components
				 and other parts of the application which can be re-used throught the app or perhaps help in refactoring 
				 conde within a container.

**src/containers** - Currently holds a class based component serving as our main container. 

**src/util** - Holds helper functions


##Future enhancements
- Add recursion perhaps when evaluating / validating the expression.
- Add a button in the calculator which will clear the user input.
- Add a button in the calculator UI to remove user input one string at a time.
- Add a decimal point in the calculator UI so that users are also able to insert decimal numbers.
- The UI calculator buttons can be grouped so that it gives more look and feel of 
  a real calculator. This can also be done with the help of flexbox >>> Ex: numbers 0 - 9 
  in the calculator UI can be displayed seperately giving them a reverse order. 
  Operators " + , - , * ,  / , sin , cos , tan " can be grouped and displayed in a column(s)    


- Code Improvements:
	- re-factor the code so that content the Calculator.js container is more concise. This can be done by 
	  creating a component for the Calculator section and another component for the Valid Results section.
- Styling improvements: 
	- Add a CSS pre-processor such as SaSS
	- Enable CSS Modules. This will enable you to scope styles therefore avoiding style conflicts.
	- Improve styling for Valid Results section (where we are displaying the last 5 expressions + results)
	- Layout improvements: Perhaps with the use of flexbox place the calculator and the view results in the same row.
	  This way there is less white space when viewed from on a desktop
	- Add a logo
- Testing: 
  - Include Unit testing
  - Test on different browsers and different layouts such as mobile phones and tablets (due to time limitation, the code was only tested on Chrome).


##Other Notes
In case of any issues or further task clarification do not hesitate to send an email to yanica.fenech@gmail.com
