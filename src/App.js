import React from 'react';

import Calculator from './containers/Calculator/Calculator';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">        
        <p>
          SuprNation Code Challenge
        </p>
      </header>

      <section>
        <Calculator />
      </section>
    </div>
  );
}

export default App;
