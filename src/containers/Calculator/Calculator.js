import React, { Component } from 'react';
import axios from 'axios';

import Keypad from '../../components/Keypad/Keypad';
import { evaluateExpression, validateExpression } from '../../util/MathUtil';
import './Calculator.css';

const ALLOWED_OPERATORS = ['+', '-', '*', '/', 'sin', 'cos', 'tan'];

class Calculator extends Component {	

	state = {
		userInput : '',
    	userResult : undefined,
    	userInputError : '',
    	resultHistory: []
	}

	/*
	 * Handle user Input
	 * Call upon evaluateExpression method which executes the evaluate method provided by the math.js library
	 * and updates the state accordingly.
	 * 
	 * @param event
	*/
	handleInput = (event) => {
		//set the input value to be equal to the event if there is one or else to the updated state value
		let val  = event ? event.target.value : this.state.userInput; 
		this.setState({userInput : val});
	    
	    if(validateExpression(val)) {
	    	try {
	        	const result =  evaluateExpression(val);
		        if(val) {	        		        		        
	        		//IF code reached here, expression is VALID ! update state and show result...
	        		this.setState({
	        			userInputError : undefined, 
	        			userResult : result,
	        			resultHistory :[val + ' = '+ result, ...this.state.resultHistory.slice(0, 4)]
	        		});
		        }	        
		    } catch(e){
		        //IF code reached here, expression is INVALID !
		        this.setState({userInputError : `The expression "${val}" is invalid or incomplete.`})
		    }	    
	    } else {
	    	this.setState({userInputError : `The expression "${val}" is invalid or incomplete.`})
	    }   	 
	}

	/*
	 * Get a Random number with get request 
	*/
	handleAddRandomNumber = () =>{
	axios.get('https://www.random.org/integers/?num=1&min=1&max=100&col=1&base=10&format=plain&rnd=new')
		.then(response => {			
			this.setState((prevState) => {
				return  {
					userInput: prevState.userInput += response.data
				}
			});		
			this.handleInput(null);	
	    }).catch(error => {
	      this.userInputError = 'Something went wrong';
	    });		
	}

	/*
	 * This Function updates the value of the displayed input element
	 * This function is called when a button from the keypad is clicked
	 *
	 * @param value
	*/
	handleKeypadPress = (value) => {
		this.setState((prevState) => {

			if(value === 'sin' || value === 'cos' || value === 'tan') {
    			value += '()';
    		}
			return  {				
				userInput: prevState.userInput += value
			}
		}, () => {
			this.handleInput(null);	
		});	
	}

    render () {
    	//rendering key pad
    	const keypadItems = [];
    	for (let i=0; i<10; i++) {
    		keypadItems.push(<Keypad key={i} value={i} handlePress={this.handleKeypadPress}/>);
    	}

    	ALLOWED_OPERATORS.map((op, index) => {    		
    		keypadItems.push(<Keypad key={'op'+ index} value={op} handlePress={this.handleKeypadPress}/>)
    	});

	    return (
	    	<div className="Calculator"> 
		      	<h1>Calculate your Mathamatical Expression</h1>
		      	<p> ** Only the following operators may be used for evaluation: 
		      		<span> +, −, ∗, /,sin, cos, tan. </span> 
		      	</p>		      	
			    
			    <div className="CalcContainer"> 
			    	<div className="Result">
			    		<label> Result : </label>
			    		<span>
 			    		{(this.state.userResult && !this.state.userInputError) && `${this.state.userResult}`}
			    		</span>
			    	</div>			    	

			    	<input 
			    	type="text" 
			    	value={this.state.userInput}
			    	onChange={this.handleInput.bind(this)} />			    						 		    
					    			   
				    <div className="CalcBtns"> 
					    {keypadItems}	
					    <button className="RandomBtn"
						    type="button"
						    onClick={this.handleAddRandomNumber}> RAND
					    </button>		  
				    </div>
			    </div>
			    			    
			    <p className="ErrorMsg">
			    	{this.state.userInputError}
			    </p>

			    <div className="ValidResults">
				    <h2> Valid Results: </h2>
				    <ul>{this.state.resultHistory.map((r, index) => 
				    	<li key={index}>{r}</li>)}
				    </ul>
			    </div>				    			   
	      	</div>
	    )
    }
} 

export default Calculator;