import React from 'react';

const KeyPad = (props) => {
  return (
  	<button onClick={() => props.handlePress(props.value)}>{props.value}</button>    
  );
}

export default KeyPad;