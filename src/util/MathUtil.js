import * as math from 'mathjs'

/*
 *
 * @param exp
 */
export const evaluateExpression = (exp) => {
  return math.evaluate(exp);
}

/**
 * Validates the expression. 
 * Checks whether the user input format falls under a case 
 * returns a boolean reflecting the validity of the expression 
 * 
 * @param exp
 */
export const validateExpression = (exp) => {
    let valid = false;
    try {
        let valtree = math.parse(exp);
        if(valtree && valtree.args){
            valtree.forEach(function (node) {
                switch (node.type) {
                    case 'OperatorNode':
                        valid = true;
                        break;
                    case 'ConstantNode':
                        valid = true;
                        break;
                    case 'SymbolNode':
                        valid = node.name === 'sin' || node.name === 'cos' || node.name === 'tan';
                        break;
                    case 'FunctionNode':
                        valid = true;
                        break;
                }
            });
        } else {
            valid = false;
        }
    } catch (e) {
        valid = false;
    }
    return valid;
}    