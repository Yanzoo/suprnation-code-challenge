# SuprNation Code Challenge


- Assuming that configuring the machine was not a priority of this exercise, I have used the create-react-app tool 
 to create the React App.
- Decimal numbers where not specified. Currently, the decimal point does not make part of the calculator UI.  
  I have added this in the future exhancements section. NOTE manually inserting a number with a decimal point still works.
